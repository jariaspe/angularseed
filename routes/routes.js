var passport = require('passport'),
    Account = require('../models/account');

module.exports = function (app) {
    
    app.get('/', function (req, res) {
        res.render('login', { user : req.user });
    });

    app.post('/login', passport.authenticate('local'), function(req, res) {
        console.log('ok login');
        res.redirect('http://localhost:3000/index.html');
    });

    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });
    
};